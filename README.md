# Firefighting Simulator #

Trabalho de implementação de um simulador de combate ao fogo e resgate de vítimas, utilizando conceitos de programação paralela, tais quais, threads e semáforos.

## Descrição ##

O trabalho consiste em implementar um sistema de simulação de combate ao fogo e resgate de vítimas. O ambiente será composto por indivíduos (threads) com diferentes objetivos:

* Bombeiro: deslocam-se aleatoriamente procurando por vítimas ou por fogo. Caso perceba fogo em seu campo de percepção, vai até ele e o apaga. Quando um bombeiro localiza uma vítima, ele trata de conduzi-la imediatamente para uma ambulância.
* Ambulância: simulam um grupo de paramédicos que podem prestar socorro às vitimas menos graves. Eles ficam aguardando que os bombeiros se encarreguem de trazer as vítimas.
* Refugiado: deslocam-se aleatoriamente pelo ambiente.
* Vítimas: quando um refugiado aproxima-se do fogo ele transforma-se em uma vítima. A vítima precisa ser resgatada e tratada o mais rápido possível (entre 50 e 100 unidades de tempo por exemplo), caso contrário ela morre. Quando uma vítima é atendida, ela se volta a ser um refugiado.
* Fogo: surge aleatoriamente no ambiente.

Os indivíduos compartilham um ambiente (como uma matriz, por exemplo) podendo se deslocar de acordo com seus objetivos. Cada classe de indivíduos deve, no início da simulação, estar distribuída espacialmente no ambiente.

A interface gráfica deve apresentar o ambiente e indivíduos (threads) posicionados em cada célula. A cada passo da simulação a interface deve apresentar informações como: tempo transcorrido (em unidades de tempo), o número de vítimas salvas até o momento, o número de vítimas fatais, bombeiros e ambulâncias, além de outras informações julgadas necessárias.

## Implementação ##

Para desenvolver o trabalho, foi utilizada a linguagem Python, versão 3.7.

## Execução ##

Para execução execute ```python  firefighting-simulator/app.py```.

## Testes ##

O nome dos arquivos de teste seguem o padrão ```[nome_da_classe]_test.py```, para o nome dos métodos, é utilizado o padrão ```test_[método]```.

### Execução ###

Para executar os testes, utilize o comando ```python -m unittest discover -s tests -p '*_test.py'``` no root do diretório.
