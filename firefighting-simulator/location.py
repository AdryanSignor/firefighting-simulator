from threading import Semaphore

class Location:

    def __init__(self, x: int, y: int):
        self._x = x
        self._y = y
        self._entity = None
        self._sem = Semaphore()

    def setEntity(self, entity):
        self._entity = entity

    def getEntity(self):
        return self._entity

    def getX(self) -> int:
        return self._x

    def getY(self) -> int:
        return self._y

    def isAvaliable(self) -> bool:
        return self._entity == None

    def lock(self):
        self._sem.acquire()

    def unlock(self):
        self._sem.release()

    def getAreaPositions(self, maxX: int, maxY: int):
        if self._x == 0:
            if self._y == 0:
                positions = self._positionsFromTopLeft()
            elif self._y == maxY:
                positions = self._positionsFromTopRight()
            else:
                positions = self._positionsFromTop()

        elif self._x == maxX:
            if self._y == 0:
                positions = self._positionsFromBottomLeft()
            elif self._y == maxY:
                positions = self._positionsFromBottomRight()
            else:
                positions = self._positionsFromBottom()

        elif self._y == 0:
            positions = self._positionsFromLeft()

        elif self._y == maxY:
            positions = self._positionsFromRight()

        else:
            positions = self._positionsFromCenter()

        return positions

    # X O _
    # O O _
    # _ _ _
    def _positionsFromTopLeft(self):
        return [[0, 1], [0, 1]]

    # _ O X
    # _ O O
    # _ _ _
    def _positionsFromTopRight(self):
        return [[0, 1], [-1, 0]]

    # O X O
    # O O O
    # _ _ _
    def _positionsFromTop(self):
        return [[0, 1], range(-1, 2)]

    # _ _ _
    # O O _
    # X O _
    def _positionsFromBottomLeft(self):
        return [[-1, 0], [0, 1]]

    # _ _ _
    # _ O O
    # _ O X
    def _positionsFromBottomRight(self):
        return [[-1, 0], [-1, 0]]

    # _ _ _
    # O O O
    # O X O
    def _positionsFromBottom(self):
        return [[-1, 0], range(-1, 2)]

    # O O _
    # X O _
    # O O _
    def _positionsFromLeft(self):
        return [range(-1, 2), [0, 1]]

    # _ O O
    # _ O X
    # _ O O
    def _positionsFromRight(self):
        return [range(-1, 2), [-1, 0]]

    # O O O
    # O X O
    # O O O
    def _positionsFromCenter(self):
        return [range(-1, 2), range(-1, 2)]