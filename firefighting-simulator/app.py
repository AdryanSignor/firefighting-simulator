import time
import os
from pandas import *

from entity.entity_base import *
from entity.ambulance import *
from entity.fire import *
from entity.firefighting import *
from entity.refugee import *
from entity.victim import *
from locations import *

class App:

    AMBULANCE = {'class': Ambulance, 'qty': 2}
    FIRE = {'class': Fire, 'qty': 10}
    FIREFIGHTING = {'class': Firefighting, 'qty': 3}
    REFUGEE = {'class': Refugee, 'qty': 5}

    def __init__(self):
        self.size = self.weight, self.height = 640, 400
        self._locations = Locations(10, 10)
        self._time = 0
        self._totalDeath = 0

    def init(self):
        for entityInfo in [App.AMBULANCE, App.FIRE, App.FIREFIGHTING, App.REFUGEE]:
            for x in range(0, entityInfo['qty']):
                entity = entityInfo['class'](self._locations)
                entity.start()

    def loop(self):
        if self._time % 4 == 0:
            totalFire = 0
            totalRefugee = 0
            for location in self._locations.getAll():
                if isinstance(location.getEntity(), Fire):
                    totalFire += 1
                elif isinstance(location.getEntity(), Refugee) or isinstance(location.getEntity(), Victim):
                    totalRefugee += 1

            for x in range(totalFire, App.FIRE['qty']):
                entity = Fire(self._locations)
                entity.start()

            for x in range(totalRefugee, App.REFUGEE['qty']):
                entity = Refugee(self._locations)
                entity.start()

    def render(self):
        totalAmbulance = 0
        totalFire = 0
        totalFirefighting = 0
        totalRefugee = 0
        totalVictim = 0
        totalSaved = 0

        simulation = ['╔' + '═══╦' * self._locations.getMaxY() + '═══╗']
        for x in range(0, self._locations.getMaxX() + 1):
            row = []
            for y in range(0, self._locations.getMaxY() + 1):
                entity = self._locations.find(x, y).getEntity()
                if entity == None:
                    draw = ' '
                else:
                    draw = entity.draw()
                    if isinstance(entity, Ambulance):
                        totalAmbulance += 1
                        totalSaved += entity.getCountVictimSaved()
                    elif isinstance(entity, Fire):
                        totalFire += 1
                    elif isinstance(entity, Firefighting):
                        totalFirefighting += 1
                    elif isinstance(entity, Refugee):
                        totalRefugee += 1
                    else:
                        location = entity.getLocation()

                        if entity.isDead():
                            draw = ' '
                            self._totalDeath += 1
                            location.lock()
                            location.setEntity(None)
                            entity.setLocation(None)

                        else:
                            totalVictim += 1

                        location.unlock()

                row.append(' ' + draw + ' ')

            simulation.append('║' + '║'.join(row) + '║')

            if x == self._locations.getMaxX():
                startRowSeparator = '╚'
                middleRowSeparator = '╩'
                endRowSeparator = '╝'
            else:
                startRowSeparator = '╠'
                middleRowSeparator = '╬'
                endRowSeparator = '╣'

            simulation.append(startRowSeparator + ('═' * 3 + middleRowSeparator) * self._locations.getMaxY() + '═' * 3 + endRowSeparator)

        os.system('clear')
        print('╔' + '═' * 100 + '╗')
        print('║ Tempo da simulação:', str(self._time) + (' ' * (79 - len(str(self._time)))) + '║')
        print('╠' + '═' * 16 + '╦' + '═' * 14 + '╦' + '═' * 10 + '╦' + '═' * 15 + '╦' + '═' * 12 + '╦' + '═' * 28 + '╣')
        print('║ [A] Ambulância ║ [B] Bombeiro ║ [F] Fogo ║ [R] Refugiado ║ [V] Vítima ║ [Z] Bombeiro com vítima' + ' ' * 4 + '║')
        print('╚' + '═' * 16 + '╩' + '═' * 14 + '╩' + '═' * 10 + '╩' + '═' * 15 + '╩' + '═' * 12 + '╩' + '═' * 28 + '╝')

        print('\n'.join(simulation))

        print('╔' + '═' * 15 + '╦' + '═' * 11 + '╦' + '═' * 14 + '╦' + '═' * 15 + '╦' + '═' * 12 + '╦' + '═' * 21 + '╦' + '═' * 21 + '╗')
        print('║Ambulâncias: ' + str(totalAmbulance) + ' ║ Fogos:' + ' ' * (3 - len(str(totalFire))) + str(totalFire) + ' ║ Bombeiros:', totalFirefighting, '║ Refugiados:', totalRefugee, '║ Vítimas:', totalVictim, '║ Vítimas salvas:' + ' ' * (4 - len(str(totalSaved))) + str(totalSaved) + ' ║ Vítimas fatais:' + ' ' * (4 - len(str(self._totalDeath))) + str(self._totalDeath) + ' ║')
        print('╚' + '═' * 15 + '╩' + '═' * 11 + '╩' + '═' * 14 + '╩' + '═' * 15 + '╩' + '═' * 12 + '╩' + '═' * 21 + '╩' + '═' * 21 + '╝')

    def execute(self):
        self.init()
        while True:
            self._time += 1
            self.loop()
            self.render()
            time.sleep(2)

if __name__ == '__main__':
    App().execute()