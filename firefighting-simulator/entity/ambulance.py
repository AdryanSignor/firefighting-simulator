from entity.entity_base import *

class Ambulance(EntityBase):

    def __init__(self, locations):
        super().__init__(locations)
        self._countVictimSaved = 0

    def saveVictim(self, victim):
        self._countVictimSaved += 1
        victim.setActive(False)

    def getCountVictimSaved(self):
        return self._countVictimSaved

    def draw(self):
        return 'A'