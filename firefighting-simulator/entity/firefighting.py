from entity.ambulance import *
from entity.fire import *
from entity.victim import *
from entity.walker import *

class Firefighting(Walker):

    def __init__(self, locations):
        super().__init__(locations)
        self._victim = None
        self._countVictimSaved = 0
        self._countFireExtinguished = 0

    def actionForCloseEntity(self, entityFound):
        if isinstance(entityFound, Fire):
            self.extinguishFire(entityFound)
        elif self._victim == None and isinstance(entityFound, Victim):
            self.rescueVictim(entityFound)
        elif self._victim != None and isinstance(entityFound, Ambulance):
            self.saveVictim(entityFound)

    def extinguishFire(self, fire):
        fire.getLocation().lock()

        if fire.isActive():
            fire.getLocation().setEntity(None)
            fire.setActive(False)
            self._countFireExtinguished += 1

        fire.getLocation().unlock()

    def rescueVictim(self, victim):
        victimLocation = victim.getLocation()
        victimLocation.lock()
        if victim.getLocation() != None:
            victim.setSaving(True)
            victim.getLocation().setEntity(None)
            victim.clearLocation()
            self._victim = victim

        victimLocation.unlock()

    def saveVictim(self, ambulance):
        ambulance.getLocation().lock()
        ambulance.saveVictim(self._victim)
        ambulance.getLocation().unlock()
        self._countVictimSaved += 1
        self._victim = None

    def getCountVictimSaved(self) -> int:
        return self._countVictimSaved

    def getCountFireExtinguished(self) -> int:
        return self._countFireExtinguished

    def draw(self):
        if self._victim == None:
            return 'B'
        else:
            return 'Z'