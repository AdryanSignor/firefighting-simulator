from entity.entity_base import *

class Fire(EntityBase):

    def __init__(self, locations):
        super().__init__(locations)
        self._countVictim = 0

    def burn(self, refugee):
        refugee.setActive(False)
        self._countVictim += 1

    def draw(self):
        return 'F'