from threading import Thread

import time
import random

class EntityBase(Thread):

    def __init__(self, locations):
        super().__init__()
        self._locations = locations
        self._location = None
        self._active = True

    def run(self):
        self._location = self.spaw()
        while self._active:
            time.sleep(2)
            self.actions()

    def spaw(self):
        while True:
            x = random.randint(0, self._locations.getMaxX())
            y = random.randint(0, self._locations.getMaxY())

            location = self._locations.find(x, y)
            location.lock()

            if location.isAvaliable():
                location.setEntity(self)
                location.unlock()
                break
            else:
                location.unlock()

        return location

    def actions(self):
        pass

    def setActive(self, active: bool):
        self._active = active

    def isActive(self):
        return self._active

    def setLocation(self, location):
        self._location = location

    def getLocation(self):
        return self._location

    def clearLocation(self):
        self._location = None

    def draw(self):
        pass