from entity.entity_base import *

import random

class Walker(EntityBase):

    def actions(self):
        maxX = self._locations.getMaxX()
        maxY = self._locations.getMaxY()

        self.move(maxX, maxY)
        self.searchEntities(self._location.getAreaPositions(maxX, maxY))

    def move(self, maxX: int, maxY: int):
        positions = self._location.getAreaPositions(maxX, maxY)

        while True:
            xMove = self.randomPosition(positions[0])
            yMove = self.randomPosition(positions[1])

            if xMove != 0 or yMove != 0:
                break

        locationMove = self._locations.find(self._location.getX() + xMove, self._location.getY() + yMove)

        locationMove.lock()

        if locationMove.isAvaliable():
            self._location.lock()
            self._location.setEntity(None)
            self._location.unlock()
            self._location = locationMove
            locationMove.setEntity(self)

        locationMove.unlock()

    def randomPosition(self, positions) -> int:
        return positions[random.randint(0, len(positions) - 1)]

    def searchEntities(self, searchPositions):
        currentX = self._location.getX()
        currentY = self._location.getY()

        for x in searchPositions[0]:
            for y in searchPositions[1]:
                if x == 0 and y == 0:
                    continue

                locationSearch = self._locations.find(currentX + x, currentY + y)
                entityFound = locationSearch.getEntity()

                if entityFound != None:
                    self.actionForCloseEntity(entityFound)

    def actionForCloseEntity(self, entityFound):
        pass