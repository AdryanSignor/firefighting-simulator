from entity.fire import *
from entity.victim import *
from entity.walker import *

class Refugee(Walker):

    def actionForCloseEntity(self, entityFound):
        if isinstance(entityFound, Fire):
            self.dying(entityFound)

    def dying(self, fire):
        victim = Victim(self._locations)
        location = self._location

        location.lock()
        victim.start()
        victim.setLocation(location)
        location.setEntity(victim)
        fire.getLocation().lock()
        fire.burn(self)
        fire.getLocation().unlock()
        location.unlock()

    def draw(self):
        return 'R'