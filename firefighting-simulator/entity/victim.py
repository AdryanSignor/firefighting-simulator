from entity.entity_base import *

class Victim(EntityBase):

    def __init__(self, locations):
        super().__init__(locations)
        self._saving = False
        self._life = 30
        self._dead = False

    def spaw(self):
        return None

    def die(self):
        self._dead = True

    def isDead(self):
        return self._dead;

    def draw(self):
        return 'V'

    def setSaving(self, saving):
        self._saving = saving

    def actions(self):
        if not self._saving:
            self._life -= 1
            if self._life == 0:
                self.die()