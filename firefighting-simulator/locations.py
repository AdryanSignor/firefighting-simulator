from location import *

class Locations:

    def __init__(self, x: int, y: int):
        self._max_x = x - 1
        self._max_y = y - 1
        self._locations = []
        self.generate_all()

    def generate_all(self):
        for x in range(0, self._max_x + 1):
            for y in range(0, self._max_y + 1):
                self._locations.append(Location(x, y))

    def find(self, x: int, y: int) -> Location:
        return self._locations[x * (self._max_y + 1) + y]

    def size(self) -> int:
        return len(self._locations)

    def getMaxX(self) -> int:
        return self._max_x

    def getMaxY(self) -> int:
        return self._max_y

    def getAll(self):
        return self._locations