import setup

import unittest

from location import *
from locations import *

class LocationsTest(unittest.TestCase):

    def test_generate_all(self):
        locations = Locations(10, 10)

        self.assertEqual(locations.size(), 100, 'Falha na geração de matriz 10x10')

    def test_find(self):
        locations = Locations(7, 4)
        for x in range(0, 7):
            for y in range(0, 4):
                location = locations.find(x, y)

                self.assertIsInstance(location, Location, 'Não retornou objeto de Location')
                self.assertEqual(location.getX(), x, 'Falha na procura linha por linha')
                self.assertEqual(location.getY(), y, 'Falha na procura coluna por coluna')


if __name__ == "__main__":
    unittest.main()