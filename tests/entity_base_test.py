import setup

import unittest

from entity.entity_base import *
from locations import *

import time

class EntityBaseTest(unittest.TestCase):

    def test_spaw(self):
        locations = Locations(1, 1)

        entity = EntityBase(locations)
        entity.start()

        time.sleep(0.25)

        entity2 = EntityBase(locations)
        entity2.start()

        locations.find(0, 0).setEntity(None)

        time.sleep(0.25)

        self.assertEqual(locations.find(0, 0).getEntity(), entity2, 'Falha na remoção de entidade')

        entity.setActive(False)
        entity2.setActive(False)


if __name__ == "__main__":
    unittest.main()