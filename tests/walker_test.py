import setup

import unittest

from locations import *
from entity.walker import *

class WalkerTest(unittest.TestCase):

    def test_move(self):
        locations = Locations(10, 10)
        walker = Walker(locations)

        walker.start()
        walker.setActive(False)

if __name__ == "__main__":
    unittest.main()